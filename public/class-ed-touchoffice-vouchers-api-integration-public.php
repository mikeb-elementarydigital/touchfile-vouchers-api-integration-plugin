<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://elementarydigital.co.uk
 * @since      1.0.0
 *
 * @package    Ed_Touchoffice_Vouchers_Api_Integration
 * @subpackage Ed_Touchoffice_Vouchers_Api_Integration/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Ed_Touchoffice_Vouchers_Api_Integration
 * @subpackage Ed_Touchoffice_Vouchers_Api_Integration/public
 * @author     Elementary Digital <info@elementarydigital.co.uk>
 */
class Ed_Touchoffice_Vouchers_Api_Integration_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private $api_base_url;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ed_Touchoffice_Vouchers_Api_Integration_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ed_Touchoffice_Vouchers_Api_Integration_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/ed-touchoffice-vouchers-api-integration-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ed_Touchoffice_Vouchers_Api_Integration_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ed_Touchoffice_Vouchers_Api_Integration_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/ed-touchoffice-vouchers-api-integration-public.js', array( 'jquery' ), $this->version, false );

	}

	public function set_api_base_url() {
	    $this->api_base_url = "https://api.touchoffice.net";
    }

    public function get_api_base_url() {
        return $this->api_base_url;
    }

    private function ed_touchoffice_get_all_vouchers_url() {
        return $this->get_api_base_url().'/vouchers/?TAK='.get_option('ed_touchoffice_integration_tak_key');
    }

    private function ed_touchoffice_get_voucher_by_number_url($voucher_number) {
        return $this->get_api_base_url().'/vouchers/'.$voucher_number.'?TAK='.get_option('ed_touchoffice_integration_tak_key');
    }

    private function ed_touchoffice_get_voucher_transaction_url() {
        return $this->get_api_base_url().'/voucher_transactions/?TAK='.get_option('ed_touchoffice_integration_tak_key');
	}

    private function ed_touchoffice_query_api($url, $post_fields = false, $transaction = false) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $http_headers = array(
            'X-API-KEY' => get_option('ed_touchoffice_integration_api_key'),
            'type' => 1,
        );

        $request_headers = array();

        foreach($http_headers as $pf_key => $pf) {
            $request_headers[] = $pf_key.': '.$pf;
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);

        if($transaction) {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        }

        $response = curl_exec($ch);

        return $response;
	}

    public function ed_touchoffice_get_single_voucher_from_api($voucher_number = false) {
	    $voucher_number = ($voucher_number) ? $voucher_number : $_POST["voucher_number"];
	    $get_vouchers_url = $this->ed_touchoffice_get_voucher_by_number_url($voucher_number);
        $response = $this->ed_touchoffice_query_api($get_vouchers_url);

        return $response;
	}

    public function ed_touchoffice_register_gateway_class($methods){
        $methods[] = 'WC_Gateway_ED_Touchoffice_Vouchers';
        return $methods;
    }

    public function ed_touchoffice_update_single_voucher($transaction_amount, $order_id, $voucher_number = false){
        $voucher_number = ($voucher_number) ? $voucher_number : $_POST["voucher_number"];
        $voucher_url = $this->ed_touchoffice_get_voucher_transaction_url();

        $update_headers = array(
            "type" => 1,
            "voucherNumber" => $voucher_number,
            "site" => 1,
            "saleID" => $order_id,
            "datetime" => date('Y-m-d G:i:s'),
            "amount" => $transaction_amount,
        );

        $response = $this->ed_touchoffice_query_api($voucher_url, $update_headers, true);
    }

    public function ed_check_voucher_value(){
	    $voucher_code = $_POST["voucher_code"];
	    $voucher_json = $this->ed_touchoffice_get_single_voucher_from_api($voucher_code);

        $voucher = json_decode($voucher_json);

        if(isset($voucher->vouchers[0])) {
            $used_amount = ((int) $voucher->vouchers[0]->used_amount) / 100;
            $initial_voucher_value = ((int) $voucher->vouchers[0]->value) / 100;
            $remaining_value = $initial_voucher_value - $used_amount;
            echo $remaining_value;
        }

        wp_die();
    }

}
