<div class="wrap">
    <h1>TouchOffice API Integration</h1>

    <form method="post" action="options.php">

        <?php settings_fields('ed-touchoffice-options-group'); ?>
        <?php do_settings_sections('ed-touchoffice-options-group'); ?>

        <table class="form-table">
            <tr valign="top">
                <th scope="row">TouchOffice API key</th>
                <td><input type="text" name="ed_touchoffice_integration_api_key" value="<?php echo esc_attr( get_option('ed_touchoffice_integration_api_key') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">TouchOffice TAK key</th>
                <td><input type="text" name="ed_touchoffice_integration_tak_key" value="<?php echo esc_attr( get_option('ed_touchoffice_integration_tak_key') ); ?>" /></td>
            </tr>
        </table>

        <?php submit_button(); ?>

    </form>

    <button id="ed-test-touchoffice-button">Test integration</button>
</div>