<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://elementarydigital.co.uk
 * @since             1.0.0
 * @package           Ed_Touchoffice_Vouchers_Api_Integration
 *
 * @wordpress-plugin
 * Plugin Name:       ED TouchOffice Vouchers API Integration
 * Plugin URI:        https://elementarydigital.co.uk
 * Description:       Provides integration with the TouchOffice API and enables the use of vouchers with WooCommerce.
 * Version:           1.0.0
 * Author:            Elementary Digital
 * Author URI:        https://elementarydigital.co.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ed-touchoffice-vouchers-api-integration
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('ED_TouchOffice_Vouchers_API_Integration', '1.0.0');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ed-touchoffice-vouchers-api-integration-activator.php
 */
function activate_ed_touchoffice_vouchers_api_integration() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ed-touchoffice-vouchers-api-integration-activator.php';
	Ed_Touchoffice_Vouchers_Api_Integration_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ed-touchoffice-vouchers-api-integration-deactivator.php
 */
function deactivate_ed_touchoffice_vouchers_api_integration() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ed-touchoffice-vouchers-api-integration-deactivator.php';
	Ed_Touchoffice_Vouchers_Api_Integration_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ed_touchoffice_vouchers_api_integration' );
register_deactivation_hook( __FILE__, 'deactivate_ed_touchoffice_vouchers_api_integration' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ed-touchoffice-vouchers-api-integration.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ed_touchoffice_vouchers_api_integration() {

	$plugin = new Ed_Touchoffice_Vouchers_Api_Integration();
	$plugin->run();

}
run_ed_touchoffice_vouchers_api_integration();
