<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://elementarydigital.co.uk
 * @since      1.0.0
 *
 * @package    Ed_Touchoffice_Vouchers_Api_Integration
 * @subpackage Ed_Touchoffice_Vouchers_Api_Integration/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Ed_Touchoffice_Vouchers_Api_Integration
 * @subpackage Ed_Touchoffice_Vouchers_Api_Integration/includes
 * @author     Elementary Digital <info@elementarydigital.co.uk>
 */
class Ed_Touchoffice_Vouchers_Api_Integration_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'ed-touchoffice-vouchers-api-integration',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
