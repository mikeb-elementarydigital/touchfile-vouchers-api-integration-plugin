<?php

if(!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))){
    return;
}

function ed_touchoffice_vouchers_init_class(){
    class WC_Gateway_ED_Touchoffice_Vouchers extends WC_Payment_Gateway {

        private $validated = false;
        private $validation_error_message;
        private $voucher_code;
        private $transaction_amount;

        public function __construct(){
            $this->id = 'ed_touchoffice_vouchers';
            $this->icon = apply_filters('woocommerce_gateway_icon', '/app/uploads/2018/08/ed-direct-debit.png');
            $this->has_fields = true;
            $this->method_title = 'Gift Vouchers';
            $this->method_description = 'Allows the user to pay using gift cards/vouchers supplied by TouchOffice';

            $this->init_form_fields();
            $this->init_settings();

            $this->title = $this->get_option('title');

            add_action('woocommerce_update_options_payment_gateways_'.$this->id, array($this, 'process_admin_options'));
        }

        public function init_form_fields(){
            $this->form_fields = array(
                'enabled' => array(
                    'title' => __( 'Enable/Disable', 'woocommerce' ),
                    'type' => 'checkbox',
                    'label' => __( 'Enable TouchOffice Voucher Payment', 'woocommerce' ),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title' => __( 'Title', 'woocommerce' ),
                    'type' => 'text',
                    'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
                    'default' => __( 'Pay by gift card', 'woocommerce' ),
                    'desc_tip'      => true,
                ),
                'description' => array(
                    'title' => __( 'Customer Message', 'woocommerce' ),
                    'type' => 'textarea',
                    'default' => ''
                )
            );
        }

        public function payment_fields(){
            include ABSPATH."/wp-content/plugins/ed-touchoffice-vouchers-api-integration/public/partials/ed-touchoffice-vouchers-api-integration-public-display.php";
        }

        public function process_payment($order_id){

            if(!$this->validated){
                wc_add_notice($this->validation_error_message, 'error');
                return false;
            }

            global $woocommerce;
            $order = new WC_Order( $order_id );

            $touchoffice_class = new Ed_Touchoffice_Vouchers_Api_Integration_Public('Ed_Touchoffice_Vouchers_Api_Integration_Public', 1);
            $touchoffice_class->set_api_base_url();
            $touchoffice_class->ed_touchoffice_update_single_voucher($this->transaction_amount, $order->order_key, $this->voucher_code);

            $order->update_status('wc-processing', __( 'Gift voucher payment', 'woocommerce' ));

            wc_reduce_stock_levels($order_id);
            $woocommerce->cart->empty_cart();

            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url( $order )
            );
        }

        public function validate_fields(){
            $submitted_voucher_code = $_POST['ed-touch-office-woocommerce-voucher-code'];
            $this->voucher_code = $submitted_voucher_code;
            $touchoffice_class = new Ed_Touchoffice_Vouchers_Api_Integration_Public('Ed_Touchoffice_Vouchers_Api_Integration_Public', 1);
            $touchoffice_class->set_api_base_url();

            $response_json = $touchoffice_class->ed_touchoffice_get_single_voucher_from_api($submitted_voucher_code);
            $response = json_decode($response_json);

            if(isset($response->vouchers[0])) {
                $used_amount = ((int) $response->vouchers[0]->used_amount) / 100;
                $initial_voucher_value = ((int) $response->vouchers[0]->value) / 100;
                $remaining_value = $initial_voucher_value - $used_amount;
                $cart_total = WC()->cart->get_totals();

                if($cart_total["total"] <= $remaining_value) {
                    $this->transaction_amount = $cart_total["total"] * 100;
                    $this->validated = true;
                    return true;
                }

                $this->validation_error_message = "Sorry, you don't have enough on your gift card for this purchase.";
                $this->validation_error_message .= " Remaining balance: £".$remaining_value;
                $this->validated = false;
                return false;
            }

            $this->validation_error_message = "Sorry, something went wrong - please try again later.";
            $this->validated = false;
            return false;
        }
    }
}

