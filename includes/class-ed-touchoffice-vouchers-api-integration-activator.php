<?php

/**
 * Fired during plugin activation
 *
 * @link       https://elementarydigital.co.uk
 * @since      1.0.0
 *
 * @package    Ed_Touchoffice_Vouchers_Api_Integration
 * @subpackage Ed_Touchoffice_Vouchers_Api_Integration/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ed_Touchoffice_Vouchers_Api_Integration
 * @subpackage Ed_Touchoffice_Vouchers_Api_Integration/includes
 * @author     Elementary Digital <info@elementarydigital.co.uk>
 */
class Ed_Touchoffice_Vouchers_Api_Integration_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
